let http = require('http');
const PORT = 3000;

let directory = [

	{
		"name": "Brandon",
		"email": "brandon@gmail.com"
	},
	{
		"name": "Robert",
		"email": "robert@gmail.com"

	}

];

http.createServer((req,res)=>{

	//retrieve our documents
		//url = /users
		//method = get
		//response = array of objects

		if(req.url =="/users" && req.method == "GET"){

			res.writeHead(200,{"Content-Type": "application/json"});
			res.write(JSON.stringify(directory));
			res.end();

		}

	//create a new user
		//url = /users
		//method = post
		//data from the request will be coming from client's body
			//req.body
		else if(req.url =="/users" && req.method == "POST"){

			let reqBody = "";
			req.on("data", (data)=>{

				reqBody += data;


			});

			req.on("end", () => {

				reqBody = JSON.parse(reqBody);
				console.log(reqBody);

				let newUser = {
					"name": reqBody.name,
					"email": reqBody.email
				}

				directory.push(newUser);

				console.log(directory);

				res.writeHead(200, {"Content-Type":"application/json"});
				res.write(JSON.stringify(directory));
				res.end();


			});
				
				

		}else{

			res.writeHead(404, {"Content-Type": "text/plain"});
			res.end("Request cannot be completed");
		}



}).listen(PORT);

console.log('Server now running');